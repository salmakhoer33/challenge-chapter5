const express = require ('express');
const app = express();
const PORT = 8000;
const fs = require("fs");
const data = require("./users.json")


app.use(express.json());
app.use(express.urlencoded({ extended : false}));
app.set("view engine", "ejs");
app.use(express.static("./public"));


app.get("/", (req, res) => {
    res.render ("index.ejs");
});

app.get("/games", (req, res) => {
    res.render ("games.ejs");
});

app.get("/log", (req, res) => {
    const name = "Users Email"
    res.render("login.ejs", {name});
});

app.get("/api/posts", (req, res) => {
    res.json(data);
  });


app.post("/", (req, res) => {
    res.redirect("/log");
});



app.post("/log", (req, res) => {
    const {email} = req.body

    console.log(email);

    res.redirect("/");
});

app.post("/index", (req, res) => {
    res.redirect("/games");
});


app.use(function(req, res){
    res.status(404).render('404');
});



app.listen(PORT, () => console.log(`Listening at http://localhost:${PORT}`))